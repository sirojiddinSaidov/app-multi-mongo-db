package uz.pdp.appmultimongodb.collection;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class Phone {

    @Id
    private String id;

    private String number;
}
