package uz.pdp.appmultimongodb.repository.second;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appmultimongodb.collection.Phone;
import uz.pdp.appmultimongodb.collection.Post;

@RepositoryRestResource(path = "post")
public interface PostRepository extends MongoRepository<Post, String> {
}
