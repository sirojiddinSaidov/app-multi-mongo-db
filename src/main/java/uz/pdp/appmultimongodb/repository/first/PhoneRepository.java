package uz.pdp.appmultimongodb.repository.first;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import uz.pdp.appmultimongodb.collection.Phone;

@RepositoryRestResource(path = "phone")
public interface PhoneRepository extends MongoRepository<Phone, String> {
}
