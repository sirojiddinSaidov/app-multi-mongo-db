package uz.pdp.appmultimongodb.config;

import com.mongodb.MongoClientSettings;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.mongo.MongoProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDatabaseFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDatabaseFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import static java.util.Collections.singletonList;

@Configuration
@EnableMongoRepositories(
        basePackages = "uz.pdp.appmultimongodb.repository.second",
        mongoTemplateRef = "secondMongoTemplate")
@EnableConfigurationProperties
public class SecondConfig {

    @Bean(name = "secondProperties")
    @ConfigurationProperties(prefix = "spring.data.mongodb.second")

    public MongoProperties primaryProperties() {
        return new MongoProperties();
    }


    @Bean(name = "secondMongoClient")
    public MongoClient mongoClient(@Qualifier("secondProperties") MongoProperties mongoProperties) {

//        MongoCredential credential = MongoCredential
//                .createCredential(
//                        mongoProperties.getUsername(),
//                        mongoProperties.getAuthenticationDatabase(),
//                        mongoProperties.getPassword());
//
//        return MongoClients.create(MongoClientSettings.builder()
//                .applyToClusterSettings(builder -> builder
//                        .hosts(singletonList(new ServerAddress(mongoProperties.getHost(),
//                                mongoProperties.getPort()))))
//                .credential(credential)
//                .build());
        return MongoClients.create(mongoProperties.getUri());
    }


    @Bean(name = "secondMongoDBFactory")
    public MongoDatabaseFactory mongoDatabaseFactory(
            @Qualifier("secondMongoClient") MongoClient mongoClient,
            @Qualifier("secondProperties") MongoProperties mongoProperties) {
        return new SimpleMongoClientDatabaseFactory(mongoClient, mongoProperties.getDatabase());
    }


    @Bean(name = "secondMongoTemplate")
    public MongoTemplate mongoTemplate(@Qualifier("secondMongoDBFactory") MongoDatabaseFactory mongoDatabaseFactory) {
        return new MongoTemplate(mongoDatabaseFactory);
    }
}