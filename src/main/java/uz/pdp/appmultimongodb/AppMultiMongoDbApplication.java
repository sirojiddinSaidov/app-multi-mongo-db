package uz.pdp.appmultimongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppMultiMongoDbApplication {

    public static void main(String[] args) {
        SpringApplication.run(AppMultiMongoDbApplication.class, args);
    }

}
